<%@page import="entity.Product"%>
<%@page import="dao.DAOCategory"%>
<%@page import="dao.DAOCustomer"%>
<%@page import="entity.Category"%>
<%@ page import="java.util.Base64" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page import="dao.DAOProduct,display.ProductDisplay,java.util.Vector"%>

<%
    DAOCategory daoCate = new DAOCategory();
    Vector<Category> cateList = daoCate.getAll();
    DAOProduct daoPro = new DAOProduct();
%>

<!DOCTYPE html>
<html lang="en">

    <head>
        <meta charset="utf-8">
        <title>Project</title>
        <meta content="width=device-width, initial-scale=1.0" name="viewport">

        <!-- Font lobster -->
        <link href="https://fonts.googleapis.com/css2?family=Lobster&display=swap" rel="stylesheet">


        <!-- Font-icon Boostrap -->
        <link href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.7.2/font/bootstrap-icons.css" rel="stylesheet">

        <!-- Customized Bootstrap Stylesheet -->
        <link href="css/style.css" rel="stylesheet">

        <style>
            .category-container {
                display: flex;
                align-items: center;
                justify-content: center;
                margin-right: 87px;
                margin-top: 15px;
            }

            .category-container h2 {
                margin: 0;
            }

            .category-container i {
                margin: 0 5px;
            }

            .item {
                color: #EB7100;
                font-size: 20px;
            }

            body {
                font-family: 'Lobster', cursive;
            }

            .div-img {
                display: flex;
                flex-direction: column;
                align-items: center; 
                padding: 20px;
            }
            .div-img img {
                border-radius: 10px;
                width: 600px;
                height: auto;
            }
        </style>
    </head>

    <body>

        <!-- Topbar Start -->
        <jsp:include page="topbar.jsp"></jsp:include>
            <!-- Topbar End -->

            <!-- Navbar Start -->
        <jsp:include page="navbar.jsp">
            <jsp:param name="service" value='<%=(String) (request.getAttribute("service"))%>'></jsp:param>
        </jsp:include>
        <!-- Navbar End -->


        <div style="text-align: center">
            <div class="category-container">
                <i class="bi bi-arrow-right-circle-fill item"></i>
                <h2><span style="color: rgb(0, 121, 63);" class="px-2">DISH LIST</span></h2>                
            </div>
        </div>

        <div style="width: 100%; padding: 50px 15px;">               
            <div style="display: flex" class="col-lg-12">
                <% int imageCount = 1;
                    for (Category cat : cateList){ 
                    Vector<Product> products = daoPro.getAllbyCategory(cat.getCateId());                        
                %>
                <div style="padding-bottom: 20px;" class="col-lg-4">
                    <div style="padding: 30px; border: 1px solid #ccc">
                        <p style="text-align: right"><%= products.size() %> pieces</p>                            
<!--                        <a href="ClientController?service=listShop&cateid=<%= cat.getCateId() %>">-->
                            <img style="margin-bottom: 20px; max-width: 100%; height: auto"  src="imgCate/<%=imageCount%>.jpg" alt="">
<!--                        </a>-->
                        <h5 style="font-weight: 600; margin: 0"><%= cat.getCateName() %></h5>
                    </div>
                </div>
                <% imageCount++; %>
                <%}%>
            </div>
        </div>
    </div>

    <!-- Products Start -->
    <div style="width: 100%; padding: 0 15px;">
        <div style="padding-bottom: 48px; text-align: center">
            <div class="category-container">
                <i class="bi bi-heart-fill item"></i>
                <h2><span style="color: rgb(0, 121, 63);" class="px-2">HOT THIS WEEK</span></h2>                
            </div>
        </div>
        <div style="display: flex; padding-bottom: 50px">
            <%
                Vector<ProductDisplay> vector = daoPro.getDisplay("select TOP 4 * from Product as a join Category as b on a.cateID = b.cateID ORDER BY newID()");
                for (ProductDisplay pd : vector) {
            %>
            <div >                 
                <div class="div-img">
                    <% String base64Image = Base64.getEncoder().encodeToString(pd.getImage()); %>

                    <img style="border-radius: 10px; width: 600px; height: auto;" src="data:image/jpg;base64, <%= base64Image %>" class="img-fluid"/>
                </div>
                <div style="text-align: center; padding-top: 20px; padding-bottom: 20px">
                    <h6 style="margin-bottom: 20px"><%=pd.getPname()%></h6>
                    <div style="display: flex; justify-content: center">
                        <h6><%=pd.getPriceFormat()%></h6><h6 style="color: red; margin-left: 15px"><del><%= String.format("%2.0f VND", pd.getPrice() * 2)%></del></h6>
                    </div>
                </div>
                <div style="display: flex; justify-content: space-between;">
                    <a style="color: black;" href="ClientController?service=detail&pid=<%=pd.getPid()%>"><i style="color: orange" class="bi bi-emoji-heart-eyes-fill mr-1"></i>More Information</a>
                    <a style="color: black; margin-right: 25px" href="ClientController?service=addCart&pid=<%=pd.getPid()%>"><i style="color: orange" class="bi bi-cart-check-fill mr-1"></i>Add To Cart</a>
                </div>
            </div>
            <%}%>
        </div>
    </div>
    <!-- Footer Start -->
    <jsp:include page="footer.jsp"></jsp:include>

    <script type="text/javascript">
        function arrayBufferToBase64(buffer) {
            var binary = '';
            var bytes = new Uint8Array(buffer);
            var len = bytes.byteLength;
            for (var i = 0; i < len; i++) {
                binary += String.fromCharCode(bytes[i]);
            }
            return window.btoa(binary);
        }
    </script>
</body>

</html>
