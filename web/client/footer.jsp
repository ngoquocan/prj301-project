
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <div style="background-color: #f5f5f5; width: 100%; padding: 0 15px; color: black">
        <div style="display: flex; padding-top: 60px; padding-left: 10px">
            <div style="margin-bottom: 50px; padding-right: 20px">
                <a style="text-decoration: none" href="">
                    <h1 style="margin-bottom: 20px; font-weight: 600"><span> <img style="border-radius: 45px; width: 120px;" src="imgLogo/logo.jpg"> </span>Food</h1>
                </a>
                <p>Our bread is proud to be the best bread in this northern bay</p>
                <p style="margin-bottom: 20px;"><i style="margin-right: 20px" class="bi bi-map-fill"></i>Km29 Thang Long Avenue, Ha Noi</p>                
                <p style="margin-bottom: 0"><i style="margin-right: 20px" class="bi bi-telephone-outbound-fill"></i>+19000091</p>
            </div>
            <div class="col-lg-8">
                <div style="display: flex">
                    <div style="margin-bottom: 20px" class="col-md-4">
                        <h5 style="font-weight: 600; color: black; margin-bottom: 30px;">Quick Links</h5>
                        <div style="display: flex; flex-direction: column; justify-content: start">
                            <a style="color: black; margin-bottom: 15px" href="ClientController?service=home"><i class="fa fa-angle-right mr-2"></i>Home</a>
                            <a style="color: black; margin-bottom: 15px" href="ClientController?service=listShop"><i class="fa fa-angle-right mr-2"></i>Stall</a>
                            <a style="color: black; margin-bottom: 15px" href="ClientController?service=cart"><i class="fa fa-angle-right mr-2"></i>Your order</a>                                
                            <a style="color: black; margin-bottom: 15px" href="https://www.facebook.com/profile.php?id=100041779639302"><i class="fa fa-angle-right mr-2"></i>Contact Us</a>
                        </div>
                    </div>
                    <div style="margin-bottom: 20px" class="col-md-4">
                        <h5 style="font-weight: 600; color: black; margin-bottom: 20px">License</h5>
                        <div style="display: flex; flex-direction: column; justify-content: start">
                            <a style="color: blue; margin-bottom: 5px;" href="">MXH 363/GP-BTTTT</a>
                            <a href="http://online.gov.vn/Home/WebDetails/13105"> <img style="width: 150px; height: auto; margin-left: -11px" src="imgLogo/bct.jpg"> </a>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <h5 style="font-weight: 600; color: black; margin-bottom: 20px">FPT Food application</h5>
                        <div style="margin-bottom: 10px">
                            <a href="https://apps.apple.com/us/app/deliverynow/id1137866760"> <img style="width: 125px;" src="imgLogo/appstore.jpg"> </a>
                        </div>
                        <div style="margin-bottom: 10px">
                            <a href="https://play.google.com/store/apps/details?id=com.deliverynow"> <img style="width: 118px; height: 37px; margin-left: 4px; border-radius: 6px;" src="imgLogo/playstore.jpg"> </a>
                        </div>
                        <div style="margin-bottom: 10px">
                            <a href="https://appgallery.huawei.com/#/app/C102401853"> <img style="width: 121px; height: 42px; border-radius: 8px; margin-left: 4px; margin-top: 4px;" src="imgLogo/gallery.jpg"> </a>
                        </div>
                    </div>
                </div>
            </div>
        </div> 
    </div>
</html>
