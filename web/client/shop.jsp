<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page import="display.ProductDisplay,java.util.Vector,dao.DAOCategory,entity.Category"%>
<%@ page import="java.util.Base64" %>
<%
    Vector<ProductDisplay> vector = (Vector<ProductDisplay>) request.getAttribute("productList");
    DAOCategory daoCat = new DAOCategory();
    Vector<Category> catList = daoCat.getAll();
    String query = (String) request.getAttribute("query");
    String cateid = request.getParameter("cateid");
    
%>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <title>Food</title>
        <meta content="width=device-width, initial-scale=1.0" name="viewport">

        <!-- Font lobster -->
        <link href="https://fonts.googleapis.com/css2?family=Lobster&display=swap" rel="stylesheet">

        <link href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.7.2/font/bootstrap-icons.css" rel="stylesheet">

        <!-- Customized Bootstrap Stylesheet -->
        <link href="css/style.css" rel="stylesheet">
        <style>
            .category-container {
                display: flex;
                align-items: center;
                justify-content: center;
                margin-right: 87px;
                margin-top: 15px;
            }

            .category-container h2 {
                margin: 0;
            }

            .category-container i {
                margin: 0 5px;
                margin-left: 65px;
            }

            .item {
                color: #EB7100;
                font-size: 20px;
            }

            body {
                font-family: 'Lobster', cursive;
            }
        </style>

    </head>

    <body>
        <!-- Topbar Start -->
        <jsp:include page="topbar.jsp"></jsp:include>
            <!-- Topbar End -->


            <!-- Navbar Start -->
        <jsp:include page="navbar.jsp">
            <jsp:param name="service" value='<%=(String)( request.getAttribute("service") )%>'></jsp:param>
        </jsp:include>
        <!-- Navbar End -->


        <!-- Page Header Start -->
        <div style="text-align: center">
            <div class="category-container">
                <i class="bi bi-arrow-right-circle-fill item"></i>
                <h2><span style="color: rgb(0, 121, 63);" class="px-2">OUR FOOD</span></h2>                
            </div>
        </div>

        <!-- Shop Start -->


        <!-- Shop Product Start -->
        <div class="col-md-12">
            <div class="row">
                <!-- Search Bar -->
                <div style="padding-bottom: 20px; padding-top: 20px; width: 100%">
                    <div style="display: flex; align-items: center; justify-content: center; margin-bottom: 20px; margin-left: 23px">
                        <form style="border: 1px solid" name="searchbar" action="ClientController">
                            <input type="hidden" name="service" value="listShop">
                            <div  style="display: flex" >                      
                                <input style="border: none" type="text" name="query" placeholder="Search by name" <% if (query != null) { %> value="<%=query%>" <%}%>>

                                <span style="display: flex; padding: 10px; background: #ccc">
                                    <i style="color: orange" class="bi bi-binoculars-fill"></i>
                                </span>

                            </div>
                        </form>

                        <form action="ClientController">                        
                            <select style="margin-left: 50px" name="cateName">
                                <% 
                                    for (Category cat : catList){                     
                                %>
                                <option hidden="" selected="">Choose one</option>
                                <option><%= cat.getCateName()%></option>
                                <%}%>
                            </select>
                            <input type="hidden" name="service" value="searchByCateName">
                            <input type="submit" name="submit" value="search">
                        </form>


                        <div style="margin-left: auto; min-width: 100px; border: 1px solid; text-align: center">
                            Total: <%=vector.size()%>
                        </div>
                    </div>
                </div>

                <!-- Cards -->

                <% for (ProductDisplay p : vector) { %>
                <div style="border: none; margin-bottom: 20px;" class="col-4">

                    <% String base64Image = Base64.getEncoder().encodeToString(p.getImage()); %>

                    <img style="border-radius: 10px; width: 600px; height: auto;" src="data:image/jpg;base64, <%= base64Image %>" class="img-fluid"/>

                    <div style="text-align: center; padding: 0; padding-bottom: 20px; padding-top: 20px">
                        <h6 style="margin-bottom: 20px"><%=p.getPname()%></h6>
                        <h6 style="color:red; margin-bottom: 20px">
                            <%if (p.getQuantity() == 0){%>Sold Out<%}
                                    else{%>Stock: <%=p.getQuantity()%><%}%>
                        </h6>
                        <div style="display: flex; justify-content: center">
                            <h6><%=p.getPriceFormat()%></h6>
                            <h6 style="margin-left: 15px"><del style="color: red"><%=String.format("%02.0f",p.getPrice()*2)%>VND</del></h6>
                        </div>
                    </div>
                    <div style="display: flex; justify-content: space-between; border: 1px solid #ccc">
                        <a style="color: black;" href="ClientController?service=detail&pid=<%=p.getPid()%>"><i style="color: orange;" class="bi bi-emoji-heart-eyes-fill "></i>View Detail</a>
                        <a style="color: black;" href="ClientController?service=addCart&pid=<%=p.getPid()%>" <%if (p.getQuantity()==0){%>disabled<%}%>"><i style="color: orange" class="bi bi-cart-check-fill "></i>Add To Cart</a>
                    </div>
                </div>
                <%}%>
            </div>
        </div>

        <!-- Footer Start -->
        <jsp:include page="footer.jsp"></jsp:include>


        <script type="text/javascript">
            function arrayBufferToBase64(buffer) {
                var binary = '';
                var bytes = new Uint8Array(buffer);
                var len = bytes.byteLength;
                for (var i = 0; i < len; i++) {
                    binary += String.fromCharCode(bytes[i]);
                }
                return window.btoa(binary);
            }
        </script>

    </body>
</html>
