<%@page import="entity.Customer"%>
<%@page import="dao.DAOCustomer"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%
    String service = (String) request.getParameter("service");
    System.out.println(service);
%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
        <div style="display:flex; justify-content: space-between ">
            <div style="display: flex; width:  100%; justify-content: center;">
                <a style="margin-left: 190px; color: red; padding: 7px 10px; text-decoration: none" href="ClientController?service=home">Home</a>
                <a style="color: black; padding: 7px 10px; text-decoration: none; margin: 0 50px;" href="ClientController?service=listShop">Stalls</a>
                <a style="color: black; padding: 7px 10px; text-decoration: none" href="ClientController?service=cart">Your Order</a>
            </div>

            <!-- Login -->
            <div style="display: flex;">
                <% if (session == null || session.getAttribute("cid") == null) {%>
                <a style="color: red; padding: 7px 10px; text-decoration: none" href="ClientController?service=login">Login</a>
                <a style="color: blue; padding: 7px 10px; text-decoration: none;" href="ClientController?service=register">Register</a>
                <% } else { 
                    DAOCustomer dao = new DAOCustomer();
                    String cid = (String) session.getAttribute("cid");
                    Customer cus = dao.get(cid);
                %>
                <a style="color: blue; padding: 7px 10px; text-decoration: none;">Hello, <%= cus.getCname() %></a>
                <a style="color: orange; padding: 7px 10px; text-decoration: none" href="ClientController?service=logout" class="nav-item nav-link">Logout</a>
                <%}%>
                <a style="color: red; padding: 7px 10px; text-decoration: none" href="AdminViewController">Admin</a>
            </div>
        </div>
    </body>
</html>
