<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%
    String service = (String) request.getParameter("service");
%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
        <div style="padding: 0 15px;">
            <div style="margin: 0 -15px; padding: 0 18px;">                
                <div class="col-lg-12">
                    <nav style="margin-top: 15px;">                           
                        <jsp:include page="navigationLogin.jsp">
                            <jsp:param name="current" value="<%=service%>"></jsp:param>
                        </jsp:include>
                    </nav>
                </div>
            </div>
        </div>
    </body>
</html>
