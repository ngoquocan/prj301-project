<%@page contentType="text/html" pageEncoding="UTF-8"%>

<%
    String admin = (String) session.getAttribute("admin");
%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
        <nav class="navbar navbar-light navbar-expand-lg" style="background-color: #EB7100;">
            <div class="collapse navbar-collapse" id="navbarNavAltMarkup">
                <div class="navbar-nav">
                    <a style="color: white; font-size: 18px; margin-left: 400px; margin-right: 20px;" class="nav-item nav-link" href="ClientController">Client</a>
                    <a style="color: white; font-size: 18px; margin: 0 20px;" class="nav-item nav-link" href="BillController?service=view">Bill</a>                    
                    <a style="color: white; font-size: 18px; margin: 0 20px;" class="nav-item nav-link" href="CategoryController?service=view">Category</a>
                    <a style="color: white; font-size: 18px; margin: 0 20px;" class="nav-item nav-link" href="CustomerController?service=view">Customer</a>
                    <a style="color: white; font-size: 18px; margin: 0 20px;" class="nav-item nav-link" href="ProductController?service=view">Product</a>                      
                </div>
            </div>
            <div class="text-right">
                <% if (admin != null){ %>
                <a style="color: white" href="AdminViewController?service=logout"> Logout</a>
                <%}%>
            </div>
        </nav>
    </body>
</html>
