<%@page import="entity.Category"%>
<%@page import="java.util.Vector"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%
    Vector<Category> list = (Vector<Category>) request.getAttribute("list");
    String query = (String) request.getAttribute("query");
    Integer s = (Integer) request.getAttribute("query_status");
    if (s == null) {
        s = -1;
    }
%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>View Category</title>
        <link rel="stylesheet" href="css/style.css"/>
        <!-- Font Boostrap -->
        <link href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.7.2/font/bootstrap-icons.css" rel="stylesheet">
    </head>
    <body>
        <jsp:include page="adminnavbar.jsp"></jsp:include>
            <div style="display: flex" class="mt-5">
                <table style="width: 85%; margin-right: 30px" class="table table-sm table-striped">
                    <thead style="background: #EB7100; color: white;">
                        <tr>
                            <th>CateID</th>
                            <th>Name</th>
                            <th class="text-center">Action</th>
                        </tr>
                    </thead>
                    <tbody>
                    <% for (Category cus : list) {%>
                    <tr>
                        <td scope="row"><%= cus.getCateId()%></td>
                        <td><%= cus.getCateName()%></td>                            
                        <td class="text-center"><a style="padding: 10px; color: white; background: red; border-radius: 10px;" href="CategoryController?service=delete&id=<%= cus.getCateId()%>" type="button" class="btn btn-danger">Remove</a></td>
                    </tr>
                    <%}%>
                </tbody>
            </table> 
                <div>
                <form  action="CategoryController">
                    <!-- Search -->
                    <div class="input-group mb-3">
                        <input type="hidden" name="service" value="view">
                        <input name="query" type="text" value="<%=query%>" class="form-control" placeholder="Name" aria-label="Name or Username" aria-describedby="button-addon2">
                        <div class="input-group-append">
                            <button style="background: orange; color: white; border: none" class="bi bi-binoculars-fill" type="submit" id="button-addon2">Search</button>
                        </div>
                    </div>                    
                </form>
                <a style="background: #EB7100; color: white; border-radius: 8px;" href="CategoryController?service=add" type="button" class="btn mt-2">Add New Category</a>
            </div>
        </div>
    </body>
</html>
