<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>ADMIN</title>
        <link rel="stylesheet" href="css/style.css"/>
        <!-- Font Boostrap -->
        <link href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.7.2/font/bootstrap-icons.css" rel="stylesheet">
    </head>
    <body>
        <jsp:include page="adminnavbar.jsp"></jsp:include>
        <div class="mt-3">       
            <form action="AdminViewController" method="post">
                <input type="hidden" name="service" value="login">
                <div class="form-group row">
                    <label style="text-align: right; margin-top: 200px" for="admin" class="col-4 col-form-label">Username</label> 
                    <div class="col-8">
                        <input style="width: 250px; border: 1px solid black; margin-top: 200px;" id="admin" name="admin" type="text" class="form-control" required="required">                        
                    </div>
                </div>
                <div class="form-group row">
                    <label style="text-align: right" for="password" class="col-4 col-form-label">Password</label> 
                    <div class="col-8">
                        <input style="width: 250px; border: 1px solid black" id="password" name="password" type="password" class="form-control" required="required">
                    </div>
                </div> 
                <div class="form-group row">
                    <div class="offset-4 col-8">
                        <button style="background: #EB7100; color: white; text-align: center; margin-left: 82px;" name="submit" type="submit" class="btn">Login</button>
                    </div>
                </div>
            </form>
        </div>
    </body>
</html>
