<%@page import="display.BillDisplay"%>
<%@page import="java.util.Vector"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%
    Vector<BillDisplay> list = (Vector<BillDisplay>) request.getAttribute("list");
    String query = (String) request.getAttribute("query");
    Integer s = (Integer) request.getAttribute("query_status");
    if (s == null) {
        s = -1;
    }
%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>View Bill</title>
        <link rel="stylesheet" href="css/style.css"/>
        <!-- Font Boostrap -->
        <link href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.7.2/font/bootstrap-icons.css" rel="stylesheet">
        <style>
            .input-group {
                display: flex;
                align-items: center;
            }

            .radio-group {
                margin-left: 10px; /* Để tạo khoảng cách giữa ô nhập liệu và các nút radio */
            }

            .radio-group label {
                margin-right: 10px; /* Để tạo khoảng cách giữa các nút radio */
            }
        </style>
    </head>
    <body>
        <jsp:include page="adminnavbar.jsp"></jsp:include>
            <div style="display: flex" class="mt-5">
                <table style="width: 85%; margin-right: 30px" class="table table-sm table-striped">
                    <thead style="background: #EB7100; color: white;">
                        <tr>
                            <th scope="col">Bill ID</th>
                            <th scope="col">Customer Name</th>
                            <th scope="col">Customer ID</th>
                            <th scope="col">Date</th>
                            <th scope="col">Total</th>
                            <th scope="col">Status</th>
                            <th scope="col">Action</th>
                        </tr>
                    </thead>
                    <tbody>
                    <% for (BillDisplay cus : list) {%>
                    <tr>
                        <td><%= cus.getBid()%></td>
                        <td><%= cus.getCname()%>  </td>
                        <td><%= cus.getCid()%>  </td>
                        <td><%= cus.getDateCreate()%></td>
                        <td><%= cus.getTotalMoney()%></td>
                        <td><%= status(cus.getStatus()) %></td>
                        <td><a href="BillDetailController?service=viewDetail&id=<%= cus.getBid()%>" type="button" style="background: #EB7100;color: white;border-radius: 10px;width: 50px;height: 30px;text-align: center;">Detail</a></td>
                    </tr>
                    <%}%>
                </tbody>
            </table>
            <div>
                <form action="BillController">
                    <!-- Search -->
                    <div class="input-group mb-3">
                        <input type="hidden" name="service" value="searchByCusIDOrStatus">
                        <input style="border: 1px solid #ccc" name="query" type="text" value="<%=query%>" class="form-control" placeholder="CustomerID or Status" aria-label="Name or Username" aria-describedby="button-addon2">

                        <div class="input-group-append">
                            <button style="background: orange; color: white; border: none; padding: 6px 5px; margin-left: 10px" class="bi bi-binoculars-fill" type="submit" id="button-addon2">Search</button>
                        </div>
                        <div style="display: inline-block; width: 100%" class="radio-group">
                            <label><input type="radio" name="search" value="cid"> cid</label>
                            <label><input type="radio" name="search" value="status"> status</label>
                        </div>
                    </div>
                </form>           
            </div>
        </div>        
    </body>
</html>

<%!
    private String status(int s) {
        if (s == 1) {
            return "<span class=\"badge badge-pill badge-info\">Process</span>";
        } else if (s==2){
            return "<span class=\"badge badge-pill badge-success\">Done</span>";
}
        return "<span class=\"badge badge-pill badge-warning\">Wait</span>";
    }
%>