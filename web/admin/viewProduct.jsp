<%@page import="display.ProductDisplay"%>
<%@page import="java.util.Vector"%>
<%@ page import="java.util.Base64" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%
    Vector<ProductDisplay> list = (Vector<ProductDisplay>) request.getAttribute("list");
    String query = (String) request.getAttribute("query");
    Integer s = (Integer) request.getAttribute("query_status");
    if (s == null) {
        s = -1;
    }
%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>View Product</title>
        <link rel="stylesheet" href="css/style.css"/>
        <!-- Font Boostrap -->
        <link href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.7.2/font/bootstrap-icons.css" rel="stylesheet">      
        <style>
            .action-buttons {
                display: flex;
                justify-content: center; 
                align-items: center; 
            }

            .action-button {
                display: inline-block;
                padding: 8px 16px;
                border: none;
                border-radius: 5px;
                text-decoration: none;
                color: white;
                font-weight: bold;
                cursor: pointer;
                margin: 0 5px; 
                background-color: #007bff; 
            }

            .delete-button {
                background-color: #dc3545; 
            }

            .update-button {
                margin-left: 10px; 
            }

        </style>
    </head>
    <body>
        <jsp:include page="adminnavbar.jsp"></jsp:include>
            <div class="row mt-5">
                <div class="col-9">
                    <table class="table table-sm table-striped">
                        <thead style="background: #EB7100; color: white;">
                            <tr>
                                <th>PID</th>
                                <th>Name</th>
                                <th>Category</th>
                                <th>Quantity</th>
                                <th>Price</th>
                                <th>Image</th>
                                <th>Description</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody>
                        <% for (ProductDisplay cus : list) {%>
                        <tr>
                            <td scope="row"><%= cus.getPid()%></td>
                            <td><%= cus.getPname()%></td>
                            <td><%= cus.getCate()%>  
                            </td>
                            <td><%= cus.getQuantity()%></td>
                            <td><%= cus.getPriceFormat()%></td>
                            <td>                                
                                <% String base64Image = Base64.getEncoder().encodeToString(cus.getImage()); %>

                                <img style="border-radius: 10px; width: 120px" src="data:image/jpg;base64, <%= base64Image %>" class="img-fluid"/>
                            </td>
                            <td><%= cus.getDescription()%></td>
                            <td> 
                                <div class="action-buttons">
                                    <a style="text-decoration: none" href="ProductController?id=<%= cus.getPid()%>&&service=delete" class="action-button delete-button">Delete</a> 
                                    <a style="text-decoration: none" href="ProductController?id=<%= cus.getPid()%>&&service=update" class="action-button update-button">Update</a>
                                </div>
                            </td>
                        </tr>
                        <%}%>
                    </tbody>
                </table>
            </div>
            <div class="col-3"> 
                <form action="ProductController">
                    <!-- Search -->
                    <div class="input-group mb-3">
                        <input type="hidden" name="service" value="view">
                        <input name="query" type="text" value="<%=query%>" class="form-control" placeholder="Name" aria-label="Name" aria-describedby="button-addon2">
                        <div class="input-group-append">
                            <button style="background: orange; color: white; border: none" class="bi bi-binoculars-fill" type="submit" id="button-addon2"></button>
                        </div>
                    </div>
                </form>
                <a style="background: #EB7100; color: white; border-radius: 8px;" href="ProductController?service=add" type="button" class="btn mt-2">Add New Product</a>

            </div>
        </div>
        <script type="text/javascript">
            function arrayBufferToBase64(buffer) {
                var binary = '';
                var bytes = new Uint8Array(buffer);
                var len = bytes.byteLength;
                for (var i = 0; i < len; i++) {
                    binary += String.fromCharCode(bytes[i]);
                }
                return window.btoa(binary);
            }
        </script>
    </body>
</html>
