<%@page import="entity.Customer"%>
<%@page import="java.util.Vector"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%
    Vector<Customer> list = (Vector<Customer>) request.getAttribute("list");
    String query = (String) request.getAttribute("query");
    Integer s = (Integer) request.getAttribute("query_status");
    if (s == null) {
        s = -1;
    }
%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>View Customer</title>
        <link rel="stylesheet" href="css/style.css"/>
        <!-- Font Boostrap -->
        <link href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.7.2/font/bootstrap-icons.css" rel="stylesheet">
    </head>
    <body>
        <jsp:include page="adminnavbar.jsp"></jsp:include>
        <div style="display: flex" class="mt-5">
            <table style="width: 85%; margin-right: 30px" class="table table-sm table-striped">
                    <thead style="background: #EB7100; color: white;">
                        <tr>
                            <th scope="col">Name</th>
                            <th scope="col">Username</th>
                            <th scope="col">Password</th>
                            <th scope="col">Address</th>
                            <th scope="col">Phone</th>
                            <th scope="col" colspan="2" class="text-center">Action</th>
                        </tr>
                    </thead>
                    <tbody>
                        <% for (Customer cus : list) {%>
                        <tr>
                            <td><%= cus.getCname()%></td>
                            <td><%= cus.getUsername()%>  
                            </td>
                            <td><%= cus.getPassword()%></td>
                            <td><%= cus.getAddress()%></td>
                            <td><%= cus.getPhone()%></td>                            
                            <td class="text-center"><a style="padding: 10px; color: white; background: red; border-radius: 10px;" href="CustomerController?go=delete&id=<%= cus.getCid()%>" type="button" class="btn btn-danger">Remove</a></td>
                        </tr>
                        <%}%>
                    </tbody>
                </table>
 
                    <form style="width: 25%" action="CustomerController">
                    <!-- Search -->
                    <div class="input-group mb-3">
                        <input type="hidden" name="service" value="view">
                        <input name="query" type="text" value="<%=query%>" class="form-control" placeholder="Name or Username" aria-label="Name or Username" aria-describedby="button-addon2">
                        <div class="input-group-append">
                            <button style="background: orange; color: white; border: none" class="bi bi-binoculars-fill" type="submit" id="button-addon2">Search</button>
                        </div>
                    </div>          
                </form>
<!--                <a style="background: #EB7100; color: white; border-radius: 8px;" href="CustomerController?service=add" type="button" class="btn mt-2">Add New Customer</a>-->
        </div>       
    </body>
</html>
