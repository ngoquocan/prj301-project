<%@page import="display.BillDisplay"%>
<%@page import="entity.Bill"%>
<%@page import="display.BillDetailDisplay"%>
<%@page import="java.util.Vector"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%
    Vector<BillDetailDisplay> list = (Vector<BillDetailDisplay>) request.getAttribute("list");
    BillDisplay bill = (BillDisplay) request.getAttribute("bill");
    double sum = 0;
%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Bill Detail</title>
        <link rel="stylesheet" href="css/style.css"/>

        <style>
            .container {
                border: 1px solid;
                padding: 30px;
                text-align: center;
                width: fit-content;
                margin-top: 130px;
            }

            .container_bill {
                width: 350px;
            }
        </style>
    </head>
    <body>
        <jsp:include page="adminnavbar.jsp"></jsp:include>
            <div class="container">
                <div class="row">                    
                    <div class="col-6">
                        <div class="container_bill">
                            <h5>Bill ID: <%= bill.getBid()%></h5>
                        <h5>Customer Name: <%= bill.getCname()%></h5>
                        <h5>Order Date: <%= bill.getDateCreate()%></h5>
                        <h5>Customer Phone: <%= bill.getRecPhone()%></h5>
                        <h5>Customer Address: <%= bill.getRecAddress()%></h5>
                        <h5>Current Bill Status: <%= status(bill.getStatus())%></h5>
                        <form action="BillController" method="post">
                            <input type="hidden" name="service" value="updateStatus">
                            <input type="hidden" name="bill" value="<%= bill.getBid()%>">
                            <div class="form-group row">
                                <label for="status" class="col-4 col-form-label">Bill Status</label> 
                                <div class="col-8">
                                    <select id="select" name="status" class="custom-select" required="required">
                                        <% if (bill.getStatus() <= 0) {%><option value="0" <%= bill.getStatus() == 0 ? "selected" : ""%>>Wait</option><%}%>
                                        <% if (bill.getStatus() <= 1) {%><option value="1" <%= bill.getStatus() == 1 ? "selected" : ""%>>Process</option><%}%>
                                        <% if (bill.getStatus() <= 2) {%><option value="2" <%= bill.getStatus() == 2 ? "selected" : ""%>>Done</option><%}%>
                                    </select>
                                </div>
                                <button style="background: #EB7100; color: white; border-radius: 8px; border: none; margin-top: 20px; margin-left: 136px;" type="submit">Update Status</button>
                            </div> 
                        </form>
                        <% if (bill.getStatus() == 0) {%>
                        <a style="padding: 10px; color: white; background: red; border-radius: 10px;" href="BillController?service=deleteBill&id=<%= bill.getBid()%>">Remove Bill</a>
                        <%}%>
                    </div>
                </div>
            </div>
        </div>
        
    </body>
</html>

<%!
    private String status(int s) {
        if (s == 1) {
            return "<span class=\"badge badge-pill badge-info\">Process</span>";
        } else if (s == 2) {
            return "<span class=\"badge badge-pill badge-success\">Done</span>";
        }
        return "<span class=\"badge badge-pill badge-warning\">Wait</span>";
    }
%>