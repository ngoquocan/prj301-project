<%@page import="java.util.Vector"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%
    Vector<String> errors = (Vector<String>) request.getAttribute("error");
    Vector<String> successes = (Vector<String>) request.getAttribute("success");
%>
<!DOCTYPE html>
<html lang="en">

    <head>
        <meta charset="utf-8">
        <title>FPT Shop</title>
        <meta content="width=device-width, initial-scale=1.0" name="viewport">

        <!-- Font lobster -->
        <link href="https://fonts.googleapis.com/css2?family=Lobster&display=swap" rel="stylesheet">

        <!-- Font Boostrap -->
        <link href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.7.2/font/bootstrap-icons.css" rel="stylesheet">
       
        <!-- Customized Bootstrap Stylesheet -->
        <link href="css/style.css" rel="stylesheet">
        
        <style>
            body {
                font-family: 'Lobster', cursive;
            }
        </style>
    </head>

    <body>
        <!-- Topbar Start -->
        <jsp:include page="../client/topbar.jsp"></jsp:include>
            <!-- Topbar End -->


            <!-- Navbar Start -->
        <jsp:include page="../client/navbar.jsp">
            <jsp:param name="service" value='<%=(String) (request.getAttribute("service"))%>'></jsp:param>
        </jsp:include>
        <!-- Navbar End -->


        <!-- Login Start -->
        <div style="background-color: #ee4d2d; margin-top: 70px; margin-bottom: 70px; padding: 50px 0; height: 100%">
            <div style="display: flex; justify-content: center; align-items: center; height: 100%">
                <div class="col-xl-5">                        
                    <form action="ClientController" method="post">
                        <input type="hidden" name="service" value="register">
                        <div style="background-color: white; text-align: center; padding: 40px; background-color: white;">
                            <% if (errors != null) {
                                    for (String msg : errors) {
                            %>
                            <p style="color: red"><%=msg%></p>
                            <%}
                                        }%>                                    
                            <h3 class="mb-5">Register</h3>
                            <div style="margin-bottom: 20px">
                                <label class="form-label" for="typeEmailX-2">Username</label>
                                <input style="border: 1px solid black" type="text" name="username" id="typeEmailX-2" class="form-control form-control-lg" required/>                                        
                            </div>
                            <div style="margin-bottom: 20px">
                                <label class="form-label" for="typePasswordX-2">Password</label>
                                <input style="border: 1px solid black" type="password" name="password" id="typePasswordX-2" class="form-control form-control-lg" required/>                           
                            </div>
                            <div style="margin-bottom: 20px">
                                <label class="form-label" for="typeNameX-2">Your Name</label>
                                <input style="border: 1px solid black" type="text" name="name" id="typeNameX-2" class="form-control form-control-lg" required/>

                            </div>
                            <div style="margin-bottom: 20px">
                                <label class="form-label" for="typeAddressX-2">Address</label>
                                <input style="border: 1px solid black" type="text" name="address" id="typeAddressX-2" class="form-control form-control-lg" required/>

                            </div>
                            <div style="margin-bottom: 20px">
                                <label class="form-label" for="typePhoneX-2">Phone</label>
                                <input style="border: 1px solid black" type="text" name="phone" id="typePhoneX-2" class="form-control form-control-lg" required/>

                            </div>
                            <h6 class="mb-5"><a style="color: red" href="ClientController?service=login">Login</a></h6>
                            <button style="background: #ee4d2d; color: white; border: none; padding: 15px; font-size: 20px; width: 100%" name="submit" type="submit">Register</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>


        <!-- Footer Start -->
        <jsp:include page="../client/footer.jsp"></jsp:include>

    </body>

</html>