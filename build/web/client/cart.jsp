<%@page import="java.util.Vector"%>
<%@page import="entity.Product"%>
<%@page import="java.util.Enumeration"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>

<%
    Vector<Product> vector = (Vector<Product>) request.getAttribute("cartList");
    double subtotal = 0;
    
%>

<!DOCTYPE html>
<html lang="en">

    <head>
        <meta charset="utf-8">
        <title>Food</title>
        <meta content="width=device-width, initial-scale=1.0" name="viewport">

        <!-- Font lobster -->
        <link href="https://fonts.googleapis.com/css2?family=Lobster&display=swap" rel="stylesheet">

        <!-- Font Boostrap -->
        <link href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.7.2/font/bootstrap-icons.css" rel="stylesheet">

        <link href="css/style.css" rel="stylesheet">

        <style>
            .category-container {
                display: flex;
                align-items: center;
                justify-content: center;
                margin-right: 87px;
                margin-top: 15px;
            }

            .category-container h2 {
                margin: 0;
            }

            .category-container i {
                margin: 0 5px;
                margin-left: 65px;
            }

            .item {
                color: #EB7100;
                font-size: 20px;
            }

            body {
                font-family: 'Lobster', cursive;
            }

            .button-container {
                display: flex;
                flex-direction: column;
            }

            .btn {
                background: orange;
                border-radius: 5px;
                color: black;
                margin-top: 10px;
                width: 80px;
            }

            .btn:first-child {
                order: -1; /* Đặt nút "Update" lên trên */
            }
        </style>
    </head>

    <body>
        <!-- Topbar -->
        <jsp:include page="topbar.jsp"></jsp:include>

            <!-- Navbar -->
        <jsp:include page="navbar.jsp">
            <jsp:param name="service" value='<%=(String) (request.getAttribute("service"))%>'></jsp:param>
        </jsp:include>

        <!--  Header  -->
        <div style="text-align: center; display: flex; margin-left: 600px; align-items: center">            
            <i class="bi bi-arrow-right-circle-fill item"></i>
            <h2><span style="color: rgb(0, 121, 63); margin-left: 10px">Your Cart</span></h2>                           
        </div>

        <!-- Cart  -->
        <div style="padding-top: 55px; margin-bottom: 90px">
            <div style="display: flex">
                <div class="col-lg-8">
                    <form action="ClientController" method="get">                       
                        <table style="border: 1px solid #dee2e6; text-align: center;" class="table">
                            <thead style="background-color: #f5f5f5; color: black">
                                <tr>
                                    <th>Food</th>
                                    <th>Price</th>
                                    <th>Quantity</th>
                                    <th>Total</th>
                                    <th>Remove</th>
                                </tr>
                            </thead>
                            <tbody>
                                <% if (vector != null) {  %>
                                <% for (Product pro : vector) {
                                        double p = pro.getPrice() * pro.getQuantity();
                                        subtotal += p;
                                %>
                                <tr>
                                    <td><a style="color: black" href="ClientController?service=detail&pid=<%= pro.getPid()%>"><%= pro.getPname()%></a></td>
                                    <td><%= pro.getPriceFormat()%></td>
                                    <td> <input type="hidden"  name="pid_<%= pro.getPid()%>" value="<%= pro.getPid()%>"> </td>
                                    <td>
                                        <input style="width: 100px; border: 1px solid #ccc; text-align: center" type="number" name="<%= pro.getPid()%>"  value="<%= pro.getQuantity()%>">
                                    </td>
                                    <td><%= String.format("%.0f", p) + " VND"%></td>
                                    <td>
                                        <a style="color: orange; background: white; border: none; " href="ClientController?service=remove&pid=<%= pro.getPid()%>">                                                                                     
                                            <i class="bi bi-trash-fill"></i>
                                        </a>
                                    </td>
                                </tr>
                                <%
                                    }
                                }
                                %>
                            </tbody>
                        </table>
                        <div class="button-container">
                            <button style="background: orange; border-radius: 5px; color: black;" class="btn" name="service" value="updateCart">Update</button>                      
                            <button style="background: orange; border-radius: 5px; color: black; margin-top: 10px; width: 100px;" name="service" value="RemoveAll"  class="btn">Remove All</button>
                        </div>
                    </form>                           

                </div>
                <div class="col-lg-4">
                    <%
                        String price = "";
                        double shipping_raw = 0; 
                        int discount = 0;
                        int shipping = 0;
                        if (subtotal != 0) {
                            price = String.format("%2.0f", subtotal);
                            shipping_raw =  Integer.parseInt(price) * 0.08;
                            shipping = (int) (shipping_raw);
                            if (Integer.parseInt(price) >= 100000) {
                            discount = (int) (Integer.parseInt(price) * 0.05);
                            }
                        }                                           
                    %>

                    <div style="background-color: #f5f5f5; padding: 17px 10px">
                        <h4 style="font-weight: 600;">Total Order</h4>
                    </div>
                    <div style="margin-top: 5px; padding: 0 10px;">
                        <div style="display: flex; justify-content: space-between; margin-bottom: 5px; padding-top: 20px" >
                            <h6 style="font-weight: 500">Subtotal</h6>
                            <h6 style="font-weight: 500"><%=price + " VND"%></h6>
                        </div>
                        <div style="display: flex; justify-content: space-between; margin-bottom: 5px; padding: 20px 0">
                            <h6 style="font-weight: 500">Shipping</h6>
                            <h6 style="font-weight: 500"><%= shipping + " VND"%></h6>
                        </div>
                        <div style="display: flex; justify-content: space-between; margin-bottom: 5px;">
                            <h6 style="font-weight: 500">Discount</h6>
                            <h6 style="font-weight: 500"><%= discount + " VND"%></h6>
                        </div>
                    </div>
                    <div style="margin-top: 20px; padding-left: 10px; color: #EDF1FF">
                        <div style="display: flex; justify-content: space-between; margin-bottom: 5px;">
                            <h5 style="font-weight: 700">Total</h5>
                            <h5 style="font-weight: 700"><%=Integer.parseInt(price) + shipping - discount + " VND"%></h5>
                        </div>
                        <form action="ClientController">
                            <input type="hidden" name="service" value="checkout">
                            <button style="color: black; background: orange; border-radius:8px; border: none; width: 100%; padding: 14px">Checkout</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>

        <!-- Footer  -->
        <jsp:include page="footer.jsp"></jsp:include>

    </body>

</html>

