<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page import="display.ProductDisplay,java.util.Vector,java.io.IOException,dao.DAOProduct"%>
<%@ page import="java.util.Base64"%>

<%
    ProductDisplay p = (ProductDisplay) request.getAttribute("product");   
    Vector<ProductDisplay> suggestions = (Vector<ProductDisplay>) request.getAttribute("suggestions");
    String cid = (String) session.getAttribute("cid");


%>

<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <title>FPT Shop</title>
        <meta content="width=device-width, initial-scale=1.0" name="viewport">


        <!-- Font lobster -->
        <link href="https://fonts.googleapis.com/css2?family=Lobster&display=swap" rel="stylesheet">

        <!-- Font Boostrap -->
        <link href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.7.2/font/bootstrap-icons.css" rel="stylesheet">

        <!-- Customized Bootstrap Stylesheet -->
        <link href="css/style.css" rel="stylesheet">

        <style>
            .category-container {
                display: flex;
                align-items: center;
                justify-content: center;
                margin-right: 87px;
                margin-top: 15px;
            }

            .category-container h2 {
                margin: 0;
            }

            .category-container i {
                margin: 0 5px;
                margin-left: 65px;
            }

            .item {
                color: #EB7100;
                font-size: 20px;
            }

            body {
                font-family: 'Lobster', cursive;
            }
        </style>
    </head>

    <body>
        <!-- Topbar Start -->
        <jsp:include page="topbar.jsp"></jsp:include>

            <!-- Navbar Start -->
        <jsp:include page="navbar.jsp">
            <jsp:param name="service" value='<%=(String) request.getAttribute("service")%>'></jsp:param>
        </jsp:include>

        <!-- Page Header Start -->
        <div style="text-align: center">
            <div class="category-container">
                <i class="bi bi-arrow-right-circle-fill item"></i>
                <h2><span style="color: rgb(0, 121, 63);">Food Information</span></h2>                
            </div>
        </div>

        <!-- Shop Detail Start -->
        <div style="width: 100%; padding: 0 15px; padding-top: 55px">
            <div style="display: flex; padding-right: 50px">
                <div style="padding-bottom: 20px" class="col-lg-5">
                    <% String base64Image = Base64.getEncoder().encodeToString(p.getImage()); %>

                    <img style="border-radius: 10px; width: 600px; height: auto;" src="data:image/jpg;base64, <%= base64Image %>" class="img-fluid"/>
                </div>

                <div style="padding-left: 90px; padding-bottom: 50px" class="col-lg-7">
                    <p style="font-weight: 700; font-size: 30px; margin-top: -11px; color: black"><%=p.getPname()%></p>
                    <h3 style="color: #0288d1; font-weight: 500; margin-bottom: 30px"><%=p.getPriceFormat()%></h3>
                    <p style="margin-bottom: 30px"><%=p.getDescription()%></p>
                    <h6 style="color: red; font-size: 17px; margin-bottom: 20px; font-weight: 500">In stock: <%= p.getQuantity()%></h6>
                    <div style="margin-bottom: 30px; display: flex; align-items: center; padding-top: 20px" >
                        <form action="ClientController">
                            <input type="hidden" name="service" value="addCart">
                            <input type="hidden" name="pid" value="<%= p.getPid()%>">
                            Quantity:
                            <div style="width: 155px; margin-right: 30px">
                                <input style="padding: 3px; border: none; width: 100px; background: #ccc; text-align: center" type="number" name="amount" value=1 max=<%=p.getQuantity()%>>
                            </div>
                            <button style="margin-top: 10px; background-color: orange; color: black; border-radius: 8px; border: none; padding: 7px" type="submit" <%= (p.getQuantity()==0 ? "disabled" : "") %>><i class="fa fa-shopping-cart mr-1"></i> Add To Cart</button>
                        </form>
                    </div>                    
                </div>
            </div>
        </div>


    <!-- Footer Start -->
    <jsp:include page="footer.jsp"></jsp:include>
    <!-- Footer End -->


    <script type="text/javascript">
        function arrayBufferToBase64(buffer) {
            var binary = '';
            var bytes = new Uint8Array(buffer);
            var len = bytes.byteLength;
            for (var i = 0; i < len; i++) {
                binary += String.fromCharCode(bytes[i]);
            }
            return window.btoa(binary);
        }
    </script>
</body>
</html>
