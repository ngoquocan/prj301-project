<%@page import="java.util.Hashtable"%>
<%@page import="dao.DAOProduct"%>
<%@page import="java.util.Enumeration"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>

<%
    DAOProduct daoPro = new DAOProduct();
    Hashtable<String, Integer> cart = daoPro.getCart(session);
    Enumeration<String> ems = cart.keys();
    int inCart = 0;
    while (ems.hasMoreElements()) {
        String key = ems.nextElement();
        inCart += (int) cart.get(key);
    }
%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
        <style>
            .contain-fluid {
                width: 100%;
                padding-right: 15px;
                padding-left: 15px;
                margin-right: auto;
                margin-left: auto;
            }

            .inline {
                display: flex;                
                margin-right: -15px;
                margin-left: -15px;
                align-items: center;
                padding: 18px 10px;
            }
        </style>
    </head>
    <div class="contain-fluid">        
        <div style="background-color: #CD1818" class="inline">
            <div style="width: 100%; padding: 0 15px;">
                <a style="text-decoration: none;" href="ClientController">
                    <h1 style="margin: 0px;">
                        <span style="color: white; background-color: blue; padding: 1px 8px; margin-bottom: 4px; display: inline-block; transform: skewX(-15deg); border-radius: 4px 0 0 4px; font-weight: 700;">F</span>
                        <span style="color: white; background-color: rgb(255, 94, 0); padding: 8px 8px; display: inline-block; transform: skewX(-15deg); border-radius: 0 4px 4px 0; font-weight: 700;">P</span>
                        <span style="color: white; background-color: green; padding: 1px 8px; margin-top: 4px; display: inline-block; transform: skewX(-15deg); border-radius: 4px 0 0 4px; font-weight: 700;">T</span>
                        <span style="color: white; font-size: 30px; padding: 1px 8px; margin-top: 4px; display: inline-block; transform: skewX(-15deg); border-radius: 4px 0 0 4px; font-weight: 700;">Shop</span>
                    </h1>
                </a>
            </div>
            <div style="padding: 0 15px; flex-shrink: 0; flex-basis: 50%;">
                <form action="ClientController">
                    <input type="hidden" name="service" value="listShop">
                    <div style="display: flex" class="">
                        <input style="width: 100%;" type="text" name="query" placeholder="What do you want to eat ?">
                        <span style="color: white; background-color: black; font-size: 25px; border-radius: 5px; padding: 2px;">
                                <i class="bi bi-search"></i>
                        </span>                        
                    </div>
                </form>                
            </div>

            <div style="text-align: right" class="col-lg-3">
                <a style="font-size: 20px; color: white; border: 1px solid" href="ClientController?service=cart">
                    <i class="bi bi-cart4"></i>
                    <span style="font-size: 18px; margin-left: 5px"><%=inCart%></span>
                </a>
            </div>
        </div>
    </div>
</html>
