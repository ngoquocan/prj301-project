package dao;

import connect.DAOEntity;
import display.ProductDisplay;
import entity.Product;
import jakarta.servlet.http.HttpSession;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Hashtable;
import java.util.Vector;
import java.util.logging.Level;
import java.util.logging.Logger;

public class DAOProduct extends DAOEntity<Product> {

    public static final String DEFAULT_ORDER_BY = " ORDER BY Product.cateID ASC";

    @Override
    public int add(Product product) {
        int n = 0;
        String sql = "INSERT INTO [dbo].[Product]\n"
                + "           ([pid]\n"
                + "           ,[pname]\n"
                + "           ,[quantity]\n"
                + "           ,[price]\n"
                + "           ,[image]\n"
                + "           ,[description]\n"
                + "           ,[status]\n"
                + "           ,[cateID])\n"
                + "     VALUES(?,?,?,?,?,?,?,?)";
        try {
            PreparedStatement pre = conn.prepareStatement(sql);
            pre.setString(1, product.getPid());
            pre.setString(2, product.getPname());
            pre.setInt(3, product.getQuantity());
            pre.setDouble(4, product.getPrice());
            pre.setBytes(5, product.getImage());
            pre.setString(6, product.getDescription());
            pre.setInt(7, product.getStatus());
            pre.setInt(8, product.getCateID());
            n = pre.executeUpdate();
        } catch (SQLException ex) {
            Logger.getLogger(DAOProduct.class.getName()).log(Level.SEVERE, null, ex);
        }
        return n;
    }

    @Override
    public int update(Product product) {
        int n = 0;
        String sql = "UPDATE [dbo].[Product]\n"
                + "   SET [pname] = ?\n"
                + "      ,[quantity] = ?\n"
                + "      ,[price] = ?\n"
                + "      ,[image] = ?\n"
                + "      ,[description] = ?\n"
                + "      ,[status] = ?\n"
                + "      ,[cateID] = ?\n"
                + " WHERE [pid] = ?";
        try {
            PreparedStatement pre = conn.prepareStatement(sql);
            pre.setString(1, product.getPname());
            pre.setInt(2, product.getQuantity());
            pre.setDouble(3, product.getPrice());
            pre.setBytes(4, product.getImage());
            pre.setString(5, product.getDescription());
            pre.setInt(6, product.getStatus());
            pre.setInt(7, product.getCateID());
            pre.setString(8, product.getPid());
            n = pre.executeUpdate();
        } catch (SQLException ex) {
            Logger.getLogger(DAOProduct.class.getName()).log(Level.SEVERE, null, ex);
        }
        return n;
    }

    public void displayAll() {
        String sql = "select * from Customer";

        try {

            Statement state = conn.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE,
                    ResultSet.CONCUR_UPDATABLE);
            ResultSet rs = state.executeQuery(sql);
            while (rs.next()) {
                /* dataType varName = rs.getDataType("fieldName|index"); */
                String pid = rs.getString("pid");
                String Pname = rs.getString("pname");
                int quantity = rs.getInt("quantity");
                double price = rs.getDouble("price");
                byte[] image = rs.getBytes("image");
                String description = rs.getString("description");
                int status = rs.getInt("status");
                int cateId = rs.getInt("cateId");
                Product pro = new Product(pid, Pname, quantity, price, image, description, status, cateId);
                System.out.println(pro.toString());
            }
        } catch (SQLException ex) {
            Logger.getLogger(DAOCustomer.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @Override
    public Vector<Product> getAll(String sql) {
        Vector<Product> vector = new Vector<>();
        ResultSet rs = this.getData(sql);
        try {
            while (rs.next()) {
                String pid = rs.getString("pid");
                String Pname = rs.getString("pname");
                int quantity = rs.getInt("quantity");
                double price = rs.getDouble("price");
                byte[] image = rs.getBytes("image");
                String description = rs.getString("description");
                int status = rs.getInt("status");
                int cateId = rs.getInt("cateId");
                Product pro = new Product(pid, Pname, quantity, price, image, description, status, cateId);
                vector.add(pro);
            }
        } catch (SQLException ex) {
            Logger.getLogger(DAOCustomer.class.getName()).log(Level.SEVERE, null, ex);
        }
        return vector;
    }

    @Override
    public int remove(String id) {
        int n = 0;
        String del = "delete from Product where pid = ?";
        try {
            PreparedStatement pre = this.getPrep(del);
            pre.setString(1, id);
            n = pre.executeUpdate();
        } catch (SQLException ex) {
            Logger.getLogger(DAOCategory.class.getName()).log(Level.SEVERE, null, ex);
        }
        return n;
    }

    @Override
    public Vector<Product> getAll() {
        return getAll("SELECT * from Product");
    }

    public Vector<ProductDisplay> getDisplay(String sql) {
        Vector<ProductDisplay> vector = new Vector<>();
        try {
            ResultSet rs = this.getData(sql);
            while (rs.next()) {
                String pid = rs.getString("pid");
                String Pname = rs.getString("pname");
                int quantity = rs.getInt("quantity");
                double price = rs.getDouble("price");
                byte[] image = rs.getBytes("image");
                String description = rs.getString("description");
                int status = rs.getInt("status");
                String cate = rs.getString("cateName");
                int cid = rs.getInt("cateID");
                ProductDisplay pd = new ProductDisplay(cate, pid, Pname, quantity, price, image, description, status, cid);
                vector.add(pd);
            }
        } catch (SQLException ex) {
            Logger.getLogger(DAOProduct.class.getName()).log(Level.SEVERE, null, ex);
        }
        return vector;
    }

    public Vector<ProductDisplay> getDisplay(PreparedStatement statement) {
        Vector<ProductDisplay> vector = new Vector<>();
        try {
            ResultSet rs = this.getData(statement);
            while (rs.next()) {
                String pid = rs.getString("pid");
                String Pname = rs.getString("pname");
                int quantity = rs.getInt("quantity");
                double price = rs.getDouble("price");
                byte[] image = rs.getBytes("image");
                String description = rs.getString("description");
                int status = rs.getInt("status");
                String cate = rs.getString("cateName");
                int cid = rs.getInt("cateID");
                ProductDisplay pd = new ProductDisplay(cate, pid, Pname, quantity, price, image, description, status, cid);
                vector.add(pd);
            }
        } catch (SQLException ex) {
            Logger.getLogger(DAOProduct.class.getName()).log(Level.SEVERE, null, ex);
        }
        return vector;
    }

    public Vector<ProductDisplay> getDisplay() {
        return getDisplay("select * from Product as a join Category as b on a.cateID = b.cateID");
    }

    @Override
    public Vector<Product> getAll(PreparedStatement statement) {
        Vector<Product> vector = new Vector<>();
        ResultSet rs = this.getData(statement);
        try {
            while (rs.next()) {
                String pid = rs.getString("pid");
                String Pname = rs.getString("pname");
                int quantity = rs.getInt("quantity");
                double price = rs.getDouble("price");
                byte[] image = rs.getBytes("image");
                String description = rs.getString("description");
                int status = rs.getInt("status");
                int cateId = rs.getInt("cateId");
                Product pro = new Product(pid, Pname, quantity, price, image, description, status, cateId);
                vector.add(pro);
            }
        } catch (SQLException ex) {
            Logger.getLogger(DAOCustomer.class.getName()).log(Level.SEVERE, null, ex);
        }
        return vector;
    }

    public Product get(String pid) {
        try {
            PreparedStatement statement = this.getPrep("SELECT * from Product where pid = ?");
            statement.setString(1, pid);
            Vector<Product> all = this.getAll(statement);
            if (!all.isEmpty()) {
                return all.get(0);
            }
        } catch (SQLException ex) {
            Logger.getLogger(DAOProduct.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }

    public Vector<Product> getAllbyCategory(int cateID) {
        try {
            PreparedStatement statement = this.getPrep("SELECT * from Product where cateID = ?");
            statement.setInt(1, cateID);
            Vector<Product> all = this.getAll(statement);
            return all;
        } catch (SQLException ex) {
            Logger.getLogger(DAOProduct.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }

    public Vector<ProductDisplay> getProductByCateName(String cateName) {
        Vector<ProductDisplay> vector = new Vector<>();
        try {
            PreparedStatement statement = this.getPrep("select * from product p join Category c on p.cateId = c.cateID\n"
                    + "  where cateName = ?");
            statement.setString(1, cateName);            
            ResultSet rs = statement.executeQuery();
            while (rs.next()) {
                String pid = rs.getString("pid");
                String Pname = rs.getString("pname");
                int quantity = rs.getInt("quantity");
                double price = rs.getDouble("price");
                byte[] image = rs.getBytes("image");
                String description = rs.getString("description");
                int status = rs.getInt("status");
                String cate = rs.getString("cateName");
                int cid = rs.getInt("cateID");
                ProductDisplay pd = new ProductDisplay(cate, pid, Pname, quantity, price, image, description, status, cid);
                vector.add(pd);
            }
            
        } catch (SQLException ex) {
            Logger.getLogger(DAOProduct.class.getName()).log(Level.SEVERE, null, ex);
        }
        return vector;
    }

    public byte[] convertInputStreamToByteArray(InputStream inputStream) throws IOException {
        ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
        byte[] buffer = new byte[4096]; // Sử dụng một buffer có kích thước lớn hơn cho hiệu suất tốt hơn
        int bytesRead;
        while ((bytesRead = inputStream.read(buffer)) != -1) {
            outputStream.write(buffer, 0, bytesRead);
        }
        return outputStream.toByteArray();
    }

    public static Hashtable<String, Integer> getCart(HttpSession session) {
        Hashtable<String, Integer> cart = (Hashtable<String, Integer>) session.getAttribute("cart");
        if (cart == null) {
            cart = new Hashtable<>();
            session.setAttribute("cart", cart);
        }
        return cart;
    }

    public static int addtoCart(HttpSession session, String pid, int amount) {
        Hashtable<String, Integer> cart = getCart(session);
        Integer value = cart.get(pid);
        if (value == null) {
            cart.put(pid, amount);
        } else {
            cart.put(pid, value + amount);
        }
        //check amount
        int newAmount = cart.get(pid);
        if (newAmount <= 0) {
            cart.remove(pid);
            return 0;
        }
        return newAmount;
    }

    public static int removeFromCart(HttpSession session, String pid) {
        Hashtable<String, Integer> cart = getCart(session);
       
        return cart.remove(pid);
    }

    public static void setToCart(HttpSession session, String pid, int amount) {
        Hashtable<String, Integer> cart = getCart(session);
        if (amount <= 0) {
            cart.remove(pid);
        } else {
            cart.put(pid, amount);
        }

    }

    public static void clearCart(HttpSession session) {
        session.removeAttribute("cart");
    }

    public static boolean isSessionLoggedIn(HttpSession session) {
        return session.getAttribute("cid") != null;
    }

    public static boolean isSessionAdmin(HttpSession session) {
        return session.getAttribute("admin") != null;
    }

    public static void main(String[] args) {
        DAOProduct dao = new DAOProduct();

        Vector<ProductDisplay> vector = dao.getProductByCateName("Bánh mì");
        for (Product product : vector) {
            System.out.println(product.getPname());
        }
    }

}
