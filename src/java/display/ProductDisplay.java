package display;

import entity.Product;

public class ProductDisplay extends Product{
    private String cate;

    public String getCate() {
        return cate;
    }

    public ProductDisplay(String cate, String pid, String pname, int quantity, double price, byte[] image, String description, int status, int cateID) {
        super(pid, pname, quantity, price, image, description, status, cateID);
        this.cate = cate;
    }

    public void setCate(String cate) {
        this.cate = cate;
    }
}

