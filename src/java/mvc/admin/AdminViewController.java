package mvc.admin;

import dao.DAOAdmin;
import dao.DAOProduct;
import entity.Admin;
import java.io.IOException;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;


@WebServlet(name = "AdminViewController", urlPatterns = {"/AdminViewController"})
public class AdminViewController extends HttpServlet {

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        HttpSession session = request.getSession();
        //Access is permitted
        String service = request.getParameter("service");
        DAOProduct daoPro = new DAOProduct();
        if (service == null) {
            service = "home";
        }
        if (service.equals("home")) {
            if (!daoPro.isSessionAdmin(session)) {
                request.getRequestDispatcher("admin/login.jsp").forward(request, response);
                return;
            }
            request.getRequestDispatcher("ProductController?service=view").forward(request, response);
        } else if (service.equals("login")) {
            String admin = (String) request.getParameter("admin");
            String password = (String) request.getParameter("password");
            DAOAdmin dao = new DAOAdmin();
            String result = dao.login(admin, password);
            if (result == null) {
                //Fail
                request.getRequestDispatcher("admin/login.jsp").forward(request, response);
            } else {
                session.setAttribute("admin", admin);
                request.getRequestDispatcher("ProductController?service=view").forward(request, response);
            }
        } else if (service.equals("logout")) {
            session.invalidate();
            request.getRequestDispatcher("admin/login.jsp").forward(request, response);
        } else {
            response.sendRedirect("ClientController");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
