package mvc.admin;

import dao.DAOProduct;
import display.ProductDisplay;
import entity.Product;
import jakarta.servlet.http.HttpSession;
import java.io.IOException;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.MultipartConfig;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.Part;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.Vector;
import java.util.logging.Level;
import java.util.logging.Logger;


@WebServlet(name = "ProductController", urlPatterns = {"/ProductController"})
@MultipartConfig
public class ProductController extends HttpServlet {


    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        HttpSession session = request.getSession();
        DAOProduct daoPro = new DAOProduct();
        if (!daoPro.isSessionAdmin(session)) {
            request.getRequestDispatcher("admin/login.jsp").forward(request, response);
            return;
        }
        //Access is permitted
        String service = request.getParameter("service");
        if (service == null) {
            service = "view";
        }
        try {
            if (service.equals("view")) {
                view(request, response, true);
            } else if (service.equals("add")) {
                add(request, response);
            } else if (service.equals("update")) {
                update(request, response);
            } else if (service.equals("delete")) {
                delete(request, response);
            }
        } catch (SQLException ex) {
            Logger.getLogger(CustomerController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

    private void view(HttpServletRequest request, HttpServletResponse response, boolean isDirect) throws SQLException, ServletException, IOException {
        DAOProduct daoPro = new DAOProduct();
        String sql = "select * from Product as a join Category as b on a.cateID = b.cateID "
                + "WHERE a.pname like ? and a.status != ? ORDER BY pid ASC";
        PreparedStatement prep = daoPro.getPrep(sql);
        //name
        String query = request.getParameter("query");
        if (query == null) {
            query = "";
        }
        prep.setString(1, "%" + query + "%");
        request.setAttribute("query", query);
        //Status
        String status = isDirect ? request.getParameter("status") : "-1";
        int s;
        try {
            s = Integer.parseInt(status);
        } catch (NumberFormatException ex) {
            s = -1;
        }
        prep.setInt(2, s);
        request.setAttribute("query_status", s);
        Vector<ProductDisplay> all = daoPro.getDisplay(prep);
        request.setAttribute("list", all);
        request.getRequestDispatcher("admin/viewProduct.jsp").forward(request, response);
    }

    private void add(HttpServletRequest request, HttpServletResponse response) throws ServletException, SQLException, IOException {
        DAOProduct daoPro = new DAOProduct();
        String submit = request.getParameter("submit");
        if (submit == null) {
            request.setAttribute("action", "add");
            request.getRequestDispatcher("admin/formProduct.jsp").forward(request, response);
        } else {
            try {
                String pid = request.getParameter("pid");
                String pname = request.getParameter("pname");
                int quantity = Integer.parseInt(request.getParameter("quantity"));
                double price = Double.parseDouble(request.getParameter("price"));
                String description = request.getParameter("description");
                int status = Integer.parseInt(request.getParameter("status"));
                int cateid = Integer.parseInt(request.getParameter("cateid"));
                Part photo = request.getPart("image");
                byte[] image = daoPro.convertInputStreamToByteArray(photo.getInputStream());

                Product pro = new Product(pid, pname, quantity, price, image, description, status, cateid);

                int n = daoPro.add(pro);

            } catch (NumberFormatException ex) {
                
            }
            view(request, response);
        }
    }

    private void delete(HttpServletRequest request, HttpServletResponse response) throws SQLException, ServletException, IOException {
        String id = request.getParameter("id");
        if (id != null) {
            DAOProduct dao = new DAOProduct();
            int n = dao.remove(id);
        }
        view(request, response);
    }

    private void update(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException, SQLException {
        DAOProduct dao = new DAOProduct();
        String submit = request.getParameter("submit");
        if (submit == null) {
            String id = request.getParameter("id");
            Product cus = dao.get(id);
            request.setAttribute("data", cus);
            request.setAttribute("action", "update");
            request.getRequestDispatcher("admin/formProduct.jsp").forward(request, response);
        } else {
            String pid = request.getParameter("pid");
            String pname = request.getParameter("pname");
            int quantity = Integer.parseInt(request.getParameter("quantity"));
            double price = Double.parseDouble(request.getParameter("price"));
            Part photo = request.getPart("image");
            byte[] image = dao.convertInputStreamToByteArray(photo.getInputStream());
            String description = request.getParameter("description");
            int status = Integer.parseInt(request.getParameter("status"));
            int cateid = Integer.parseInt(request.getParameter("cateid"));
            Product pro = new Product(pid, pname, quantity, price, image, description, status, cateid);
            int n = dao.update(pro);
            view(request, response);
        }
    }

    private void view(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException, SQLException {
        view(request, response, false);
    }

}
