/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Servlet.java to edit this template
 */
package mvc.admin;

import dao.DAOCustomer;
import dao.DAOProduct;
import entity.Customer;
import jakarta.servlet.http.HttpSession;
import java.io.IOException;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.Vector;
import java.util.logging.Level;
import java.util.logging.Logger;




/**
 *
 * @author ADMIN
 */
@WebServlet(name = "CustomerController", urlPatterns = {"/CustomerController"})
public class CustomerController extends HttpServlet {

    
    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        HttpSession session = request.getSession();
        DAOProduct daoPro = new DAOProduct();
        if (!daoPro.isSessionAdmin(session)) {
            request.getRequestDispatcher("admin/login.jsp").forward(request, response);
            return;
        }
        //Access is permitted
        String service = request.getParameter("service");
        if (service == null) {
            service = "view";
        }
        try {
            if (service.equals("view")) {
                view(request, response, true);
            } else if (service.equals("add")) {
                add(request, response);
            } else if (service.equals("update")) {
                update(request, response);
            } else if (service.equals("delete")) {
                delete(request, response);
            }
        } catch (SQLException ex) {
            Logger.getLogger(CustomerController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

    private void view(HttpServletRequest request, HttpServletResponse response, boolean isDirect) throws ServletException, IOException, SQLException {
        DAOCustomer dao = new DAOCustomer();
        String sql = "SELECT * from Customer "
                + "WHERE (cname like ? or username like ?) "
                + "AND status != ? "
                + "ORDER BY cid";
        PreparedStatement prep = dao.getPrep(sql);
        //Query
        String query = request.getParameter("query");
        if (query == null) {
            query = "";
        }
        prep.setString(1, "%" + query + "%");
        prep.setString(2, "%" + query + "%");
        request.setAttribute("query", query);
        //Status
        String status = isDirect ? request.getParameter("status") : "-1";
        int s;
        try {
            s = Integer.parseInt(status);
        } catch (NumberFormatException ex) {
            s = -1;
        }
        prep.setInt(3, s);
        request.setAttribute("query_status", s);
        Vector<Customer> all = dao.getAll(prep);
        request.setAttribute("list", all);
        request.getRequestDispatcher("admin/viewCustomer.jsp").forward(request, response);
    }

    private void add(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException, SQLException {
        String submit = request.getParameter("submit");
        if (submit == null) {
            request.setAttribute("action", "add");
            request.getRequestDispatcher("admin/formCustomer.jsp").forward(request, response);
        } else {
            String cid = request.getParameter("cid");
            String cname = request.getParameter("cname");
            String username = request.getParameter("username");
            String password = request.getParameter("password");
            String address = request.getParameter("address");
            String phone = request.getParameter("phone");
            int status = Integer.parseInt(request.getParameter("status"));
            Customer cus = new Customer(cid, cname, username, password, address, status, phone, status);
            DAOCustomer dao = new DAOCustomer();
            int n = dao.add(cus);
            view(request, response);
        }

    }

    private void update(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException, SQLException {
        DAOCustomer dao = new DAOCustomer();
        String submit = request.getParameter("submit");
        if (submit == null) {
            String id = request.getParameter("id");
            Customer cus = dao.get(id);
            request.setAttribute("data", cus);
            request.setAttribute("action", "update");
            request.getRequestDispatcher("admin/formCustomer.jsp").forward(request, response);
        } else {
            String cid = request.getParameter("cid");
            String cname = request.getParameter("cname");
            String username = request.getParameter("username");
            String password = request.getParameter("password");
            String address = request.getParameter("address");
            String phone = request.getParameter("phone");
            int status = Integer.parseInt(request.getParameter("status"));
            int isAdmin = Integer.parseInt(request.getParameter("isAdmin"));
            Customer cus = new Customer(cid, cname, username, password, address, status, phone, isAdmin);
            int n = dao.update(cus);
            view(request, response);
        }
    }

    private void delete(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException, SQLException {
        String id = request.getParameter("id");
        if (id != null) {
            DAOCustomer dao = new DAOCustomer();
            int n = dao.remove(id);
        }
        view(request, response);
    }
    
    private void view(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException, SQLException {
        view(request, response, false);
    }
}
