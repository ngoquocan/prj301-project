/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Servlet.java to edit this template
 */
package mvc.admin;

import dao.DAOBill;
import dao.DAOBillDetail;
import dao.DAOProduct;
import display.BillDisplay;
import entity.Bill;
import jakarta.servlet.http.HttpSession;
import java.io.IOException;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.Vector;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author ADMIN
 */
@WebServlet(name = "BillController", urlPatterns = {"/BillController"})
public class BillController extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        HttpSession session = request.getSession();
        DAOProduct daoPro = new DAOProduct();
        if (!daoPro.isSessionAdmin(session)) {
            request.getRequestDispatcher("admin/login.jsp").forward(request, response);
            return;
        }
        //Access is permitted
        String service = request.getParameter("service");
        if (service == null) {
            service = "view";
        }
        try {
            if (service.equals("view")) {
                view(request, response, true);
            } else if (service.equals("delete")) {
                delete(request, response);
            } else if (service.equals("updateStatus")) {
                updateStatus(request, response);
            } else if (service.equals("searchByCusIDOrStatus")) {
                searchByCusIDOrStatus(request, response);
            } else if (service.equals("deleteBill")) {               
                deleteBill(request, response);
            } else {
                view(request, response, true);
            }
        } catch (SQLException ex) {
            Logger.getLogger(CustomerController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

    private void view(HttpServletRequest request, HttpServletResponse response, boolean isDirect) throws ServletException, IOException, SQLException {
        DAOBill dao = new DAOBill();
        String sql = "SELECT * from Bill a join Customer b on a.cid = b.cid "
                + "WHERE b.username like ? "
                + "AND a.status in (?,?,?) "
                + "ORDER BY a.cid";
        PreparedStatement prep = dao.getPrep(sql);
        //Query
        String query = request.getParameter("query");
        if (query == null) {
            query = "";
        }
        prep.setString(1, "%" + query + "%");
        request.setAttribute("query", query);
        //Status
        String status = isDirect ? request.getParameter("status") : "-1";
        boolean isSearchAll = false;
        int s = -1;
        try {
            s = Integer.parseInt(status);
            if (s >= 0 && s <= 2) {
                for (int i = 2; i < 5; i++) {
                    prep.setInt(i, s);
                }
            } else {
                isSearchAll = true;
            }
        } catch (NumberFormatException ex) {
            isSearchAll = true;
        }
        if (isSearchAll) {
            prep.setInt(2, 0);
            prep.setInt(3, 1);
            prep.setInt(4, 2);
        }
        request.setAttribute("query_status", isSearchAll ? -1 : s);
        Vector<BillDisplay> all = dao.getDisplay(prep);
        request.setAttribute("list", all);
        request.getRequestDispatcher("admin/viewBill.jsp").forward(request, response);
    }

    private void delete(HttpServletRequest request, HttpServletResponse response) throws SQLException, ServletException, IOException {
        //For bill, it must delete all billdetails first
        String id = request.getParameter("id");
        if (id != null) {
            DAOBill dao = new DAOBill();
            BillDisplay bill = dao.get(id);
            if (bill.getStatus() == 0) {
                ///Remove bill details
                PreparedStatement statement = dao.getPrep("DELETE FROM BillDetail where bid = ?");
                statement.setString(1, id);
                statement.execute();
                //Remove the bill
                int n = dao.remove(id);
            }
            view(request, response);
        }
    }

    private void deleteBill(HttpServletRequest request, HttpServletResponse response) throws SQLException, ServletException, IOException {
        //For bill, it must delete all billdetails first
        String id = request.getParameter("id");
        DAOBill daoBill = new DAOBill();
        DAOBillDetail daoBillDetail = new DAOBillDetail();
        if (id != null) {
            int n = daoBillDetail.removeBillDetail(id);
            int n1 = daoBill.removeBill(id);
            view(request, response);
        }

    }

    private void searchByCusIDOrStatus(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException, SQLException {
        DAOBill dao = new DAOBill();
        String search = request.getParameter("search");
        String query = request.getParameter("query");

        if ("cid".equals(search)) {
            Vector<BillDisplay> vector = dao.getBillByCustomerID(query);
            request.setAttribute("list", vector);
            request.setAttribute("query", query);
            request.getRequestDispatcher("admin/viewBill.jsp").forward(request, response);
            return;
        } else if ("status".equals(search)) {
            int status = -1;

            try {
                status = Integer.parseInt(query);
            } catch (NumberFormatException e) {

            }

            Vector<BillDisplay> vector = dao.getBillByStatus(status);
            request.setAttribute("list", vector);
            request.setAttribute("query", query);
            request.getRequestDispatcher("admin/viewBill.jsp").forward(request, response);
            return;
        } else if (search == null) {
            Vector<BillDisplay> vector = dao.getAllBill();
            request.setAttribute("list", vector);
            request.setAttribute("query", query);
            request.getRequestDispatcher("admin/viewBill.jsp").forward(request, response);
        }
    }

    private void view(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException, SQLException {
        view(request, response, false);
    }

    private void updateStatus(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException, SQLException {
        String s = request.getParameter("status");
        String billID = request.getParameter("bill");
        try {
            int status = Integer.parseInt(s);
            if (status >= 0 && status <= 2) {
                DAOBill dao = new DAOBill();
                Bill bill = dao.get(billID);
                if (bill != null) {
                    if (bill.getStatus() > status) {

                    } else {
                        bill.setStatus(status);
                        dao.update(bill);
                    }
                }
            }
        } catch (NumberFormatException ex) {
        }
        view(request, response);
    }
}
