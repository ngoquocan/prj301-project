/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Servlet.java to edit this template
 */
package mvc.admin;

import dao.DAOCategory;
import dao.DAOProduct;
import entity.Category;
import java.io.IOException;
import java.io.PrintWriter;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.Vector;
import java.util.logging.Level;
import java.util.logging.Logger;


/**
 *
 * @author ADMIN
 */
@WebServlet(name = "CategoryController", urlPatterns = {"/CategoryController"})
public class CategoryController extends HttpServlet {
    
    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        HttpSession session = request.getSession();
        DAOProduct daoPro = new DAOProduct();
        if (!daoPro.isSessionAdmin(session)) {
            request.getRequestDispatcher("admin/login.jsp").forward(request, response);
            return;
        }
        //Access is permitted
        String service = request.getParameter("service");
        if (service == null) {
            service = "view";
        }
        try {
            if (service.equals("view")) {
                view(request, response, true);
            } else if (service.equals("add")) {
                add(request, response);
            } else if (service.equals("update")) {
                update(request, response);
            } else if (service.equals("delete")) {
                delete(request, response);
            }
        } catch (SQLException ex) {
            Logger.getLogger(CustomerController.class.getName()).log(Level.SEVERE, null, ex);
        }
        
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

    private void view(HttpServletRequest request, HttpServletResponse response, boolean isDirect) throws SQLException, ServletException, IOException {
        DAOCategory dao = new DAOCategory();
        String sql = "SELECT * from Category WHERE cateName like ? and status != ? ORDER BY cateId ASC";
        PreparedStatement prep = dao.getPrep(sql);
        //name
        String query = request.getParameter("query");
        if (query == null) {
            query = "";
        }
        prep.setString(1, "%" + query + "%");
        request.setAttribute("query", query);
        //Status
        String status = isDirect ? request.getParameter("status") : "-1";
        int s;
        try {
            s = Integer.parseInt(status);
        } catch (NumberFormatException ex) {
            s = -1;
        }
        prep.setInt(2, s);
        request.setAttribute("query_status", s);
        Vector<Category> all = dao.getAll(prep);request.setAttribute("list", all);
        request.getRequestDispatcher("admin/viewCategory.jsp").forward(request, response);
    }

    private void add(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException, SQLException {
        String submit = request.getParameter("submit");
        if (submit == null) {
            request.setAttribute("action", "add");
            request.getRequestDispatcher("admin/formCategory.jsp").forward(request, response);
        } else {
            String cname = request.getParameter("catename");
//            int status = Integer.parseInt(request.getParameter("status"));
            Category cat = new Category(cname, 1);
            DAOCategory dao = new DAOCategory();
            int n = dao.add(cat);
            view(request, response);
        }
    }

    private void update(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException, SQLException {
        DAOCategory dao = new DAOCategory();
        String submit = request.getParameter("submit");
        if (submit == null) {
            String id = request.getParameter("id");
            Category cus = dao.get(id);
            request.setAttribute("data", cus);
            request.setAttribute("action", "update");
            request.getRequestDispatcher("admin/formCategory.jsp").forward(request, response);
        } else {
            int cateid = Integer.parseInt(request.getParameter("cateid"));
            String catename = request.getParameter("catename");
            int status = Integer.parseInt(request.getParameter("status"));
            Category cus = new Category(cateid, catename, status);
            int n = dao.update(cus);
            view(request, response);
        }
    }

    private void delete(HttpServletRequest request, HttpServletResponse response) throws SQLException, ServletException, IOException {
        String id = request.getParameter("id");
        if (id != null) {
            DAOCategory dao = new DAOCategory();
            int n = dao.remove(id);
        }
        view(request, response);
    }
    
    private void view(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException, SQLException {
        view(request, response, false);
    }
}
