package mvc.client;

import dao.DAOBill;
import dao.DAOBillDetail;
import dao.DAOCustomer;
import dao.DAOProduct;
import display.ProductDisplay;
import entity.Bill;
import entity.BillDetail;
import entity.Customer;
import entity.Product;
import java.io.IOException;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Enumeration;
import java.util.Hashtable;
import java.util.Vector;
import java.util.logging.Level;
import java.util.logging.Logger;

@WebServlet(name = "ClientController", urlPatterns = {"/ClientController"})
public class ClientController extends HttpServlet {

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        String service = request.getParameter("service");
        DAOProduct daoPro = new DAOProduct();
        //Get cart
        HttpSession session = request.getSession();

        if (service == null) {
            service = "home";
        }
        request.setAttribute("service", service);
        //List Shop
        try {
            if (service.equals("listShop")) {
                listShop(request, response);
            } else if (service.equals("detail")) {
                detail(request, response);
            } else if (service.equals("home")) {
                home(request, response);
            } else if (service.equals("cart")) {
                cart(request, response, session);
            } else if (service.equals("addCart")) {
                addCart(request, response, session);
            } else if (service.equals("updateCart")) {
                updateCart(request, response, session);
            } else if (service.equals("wipeCart")) {
                daoPro.clearCart(session);
                request.getRequestDispatcher("ClientController?service=cart");
            } else if (service.equals("checkout")) {
                checkout(request, response, session);
            } else if (service.equals("login")) {
                login(request, response);
            } else if (service.equals("logout")) {
                logout(request, response, session);
            } else if (service.equals("register")) {
                register(request, response);
            } else if (service.equals("searchByCateName")) {
                searchByCateName(request, response);
            } else if (service.equals("RemoveAll")) {
                RemoveAll(request, response, session);
            } else if (service.equals("remove")) {
                String removePid = request.getParameter("pid");

                Hashtable<String, Integer> cart = (Hashtable<String, Integer>) session.getAttribute("cart");
                
                if (cart != null) {                   
                    cart.remove(removePid);
                    session.setAttribute("cart", cart);
                }
                response.sendRedirect("ClientController?service=cart");
            }

        } catch (Exception ex) {
            Logger.getLogger(DAOProduct.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    private void listShop(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException, SQLException {
        DAOProduct daoPro = new DAOProduct();
        int params = 1;
        //Initial SQL
        String sql = "select * from Product join Category on Product.cateID = Category.cateID\n"
                + "WHERE Product.status = 1 AND Product.pname LIKE ? ";
        //Search
        String query = request.getParameter("query");
        if (query == null) {
            query = "";
        }
        request.setAttribute("query", query);
        //Category
        String cateid = request.getParameter("cateid");
        Integer id = null;
        if (cateid != null) {
            id = Integer.parseInt(cateid);
            sql += " AND Product.cateid = ?";
            params++;
        }
        //Sorting
        sql += " ORDER BY Product.cateID ASC";
        //Construct statement
        PreparedStatement statement = daoPro.getPrep(sql);
        statement.setString(1, "%" + query + "%");
        if (params >= 2) {
            statement.setInt(2, id);
        }
        Vector<ProductDisplay> productList = daoPro.getDisplay(statement);
        request.setAttribute("productList", productList);
        request.getRequestDispatcher("client/shop.jsp").forward(request, response);
    }

    private void searchByCateName(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException, SQLException {
        DAOProduct dao = new DAOProduct();
        String cateName = request.getParameter("cateName");
        Vector<ProductDisplay> vector = dao.getProductByCateName(cateName);
        request.setAttribute("productList", vector);
        request.setAttribute("cateName", cateName);
        request.getRequestDispatcher("client/shop.jsp").forward(request, response);
    }

    private void detail(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException, SQLException {
        String pid = request.getParameter("pid");
        if (pid != null) {
            DAOProduct daoPro = new DAOProduct();
            //Create statement
            PreparedStatement statement = daoPro.conn.prepareStatement("select * from Product as a join Category as b on a.cateID = b.cateID WHERE pid = ?");
            statement.setString(1, pid);
            Vector<ProductDisplay> vector = daoPro.getDisplay(statement);
            if (!vector.isEmpty()) {
                //Product 
                ProductDisplay pd = vector.get(0);
                request.setAttribute("product", pd);
                //Suggestions
                PreparedStatement statementSug = daoPro.conn.prepareStatement("select TOP 6 * from Product as a join Category as b on a.cateID = b.cateID WHERE a.status = 1 AND a.quantity > 0 AND a.pid != ? ORDER By newID()");
                statementSug.setString(1, pid);
                Vector<ProductDisplay> suggestions = daoPro.getDisplay(statementSug);
                request.setAttribute("suggestions", suggestions);
                request.getRequestDispatcher("client/detail.jsp").forward(request, response);
            }
        } else {
            listShop(request, response);
        }
    }

    private void home(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        request.getRequestDispatcher("client/index.jsp").forward(request, response);
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

    private void cart(HttpServletRequest request, HttpServletResponse response, HttpSession session) throws ServletException, IOException, SQLException {
        DAOProduct daoPro = new DAOProduct();
        if (session == null || session.getAttribute("cid") == null) {
            toLogin(request, response);
            return;
        }
        //get cart data
        Hashtable<String, Integer> cart = daoPro.getCart(session);
        Vector<Product> vector = new Vector<>();
        DAOProduct dao = new DAOProduct();
        Enumeration<String> ems = cart.keys();
        while (ems.hasMoreElements()) {
            String pid = ems.nextElement();
            Product pro = dao.get(pid);
            if (pro == null) {
                continue;
            }
            //set quantity
            pro.setQuantity((int) cart.get(pid));
            vector.add(pro);
        }
        request.setAttribute("cartList", vector);
        request.getRequestDispatcher("client/cart.jsp").forward(request, response);
    }

    private void addCart(HttpServletRequest request, HttpServletResponse response, HttpSession session) throws ServletException, IOException, SQLException {
        DAOProduct daoPro = new DAOProduct();
        if (session == null || session.getAttribute("cid") == null) {
            toLogin(request, response);
            return;
        }
        String pid = request.getParameter("pid");
        String a = request.getParameter("amount");
        int amount = a == null ? 1 : Integer.parseInt(a);
        if (amount <= 0) {
            amount = 1;
        }
        daoPro.addtoCart(session, pid, amount);
        request.getRequestDispatcher("ClientController?service=cart").forward(request, response);
    }

    private void updateCart(HttpServletRequest request, HttpServletResponse response, HttpSession session) throws ServletException, IOException, SQLException {
        DAOProduct daoPro = new DAOProduct();
        if (session == null || session.getAttribute("cid") == null) {
            toLogin(request, response);
            return;
        }
        Hashtable<String, Integer> cart = daoPro.getCart(session);
        //Check if the action is to remove an item
        String pid = request.getParameter("pid");
        //Normal Update
        Enumeration<String> ems = request.getParameterNames();
        while (ems.hasMoreElements()) {
            try {
                String key = ems.nextElement();
                int newValue = Integer.parseInt(request.getParameter(key));
                if (newValue <= 0) {
                    cart.remove(key);
                } else {
                    cart.put(key, newValue);
                }
            } catch (NumberFormatException ex) {
            }
        }

        session.setAttribute("cart", cart);
        request.getRequestDispatcher("ClientController?service=cart").forward(request, response);
    }

    private void RemoveAll(HttpServletRequest request, HttpServletResponse response, HttpSession session) throws ServletException, IOException, SQLException {
        DAOProduct daoPro = new DAOProduct();
        if (session == null || session.getAttribute("cid") == null) {
            toLogin(request, response);
            return;
        }
//        Enumeration<String> ems = request.getAttributeNames();
//        while (ems.hasMoreElements()) {
//            String key = ems.nextElement().toString();
//            if (!key.equals("username") && !key.equals("password")) {
//                session.removeAttribute(key);
//            }
//        }
        // Cập nhật giỏ hàng trống vào session
        Hashtable<String, Integer> cart = new Hashtable<>();
        session.setAttribute("cart", cart);
        request.getRequestDispatcher("ClientController?service=cart").forward(request, response);

    }

    private void checkout(HttpServletRequest request, HttpServletResponse response, HttpSession session) throws ServletException, IOException {
        DAOProduct daoPro = new DAOProduct();
        if (session == null || session.getAttribute("cid") == null) {
            toLogin(request, response);
            return;
        }
        Hashtable<String, Integer> cart = daoPro.getCart(session);
        boolean canProceedtoCheckout = true;
        Enumeration<String> ems = cart.keys();
        if (cart.isEmpty()) {
            //Cart is empty
            request.getRequestDispatcher("ClientController?service=cart").forward(request, response);
            return;
        }
        DAOProduct dao = new DAOProduct();
        while (ems.hasMoreElements()) {
            String pid = ems.nextElement();
            int amount = cart.get(pid);
            if (amount <= 0) {
                canProceedtoCheckout = false;
                continue;
            }
            //Get Product from Database
            Product pro = dao.get(pid);
            if (pro == null) {
                canProceedtoCheckout = false;
                continue;
            }
            int stock = pro.getQuantity();
            if (stock < amount) {
                canProceedtoCheckout = false;
            }
        }
        if (canProceedtoCheckout) {
            placeOrder(request, response, session);
        }
        request.getRequestDispatcher("ClientController?service=cart").forward(request, response);
    }

    private void login(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String submit = request.getParameter("submit");
        if (submit == null) {
            request.getRequestDispatcher("client/login.jsp").forward(request, response);
        } else {
            DAOCustomer dao = new DAOCustomer();
            String username = request.getParameter("username");
            String password = request.getParameter("password");
            String cid = dao.login(username, password);
            if (cid == null) {
                request.getRequestDispatcher("client/login.jsp").forward(request, response);
            } else {
                HttpSession session = request.getSession();
                session.setAttribute("cid", cid);
                response.sendRedirect("ClientController");
            }
        }
    }

    private void toLogin(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        request.getRequestDispatcher("client/login.jsp").forward(request, response);
    }

    private void logout(HttpServletRequest request, HttpServletResponse response, HttpSession session) throws ServletException, IOException {
        session.invalidate();
        home(request, response);
    }

    private void placeOrder(HttpServletRequest request, HttpServletResponse response, HttpSession session) {
        DAOProduct daoPro = new DAOProduct();
        DAOBill daoBill = new DAOBill();
        DAOBillDetail daoBd = new DAOBillDetail();
        DAOCustomer daoCus = new DAOCustomer();
        Hashtable<String, Integer> cart = daoPro.getCart(session);
        Enumeration<String> keys = cart.keys();
        //Infos
        String cid = (String) session.getAttribute("cid");
        Customer cus = daoCus.get(cid);
        //Create a new bill
        SimpleDateFormat formatter = new SimpleDateFormat("_dd/MM/yyyy_HH:mm:ss");
        String bid = cid + formatter.format(new Date());
        String address = cus.getAddress();
        String phone = cus.getPhone();
        String note = ""; // TODO 
        int status = 0;
        Bill bill = new Bill(bid, address, phone, note, status, cid);
        int n = daoBill.add(bill);
        //Total money
        double totalMoney = 0;
        //Go through all elements
        while (keys.hasMoreElements()) {
            String pid = keys.nextElement();
            int amount = cart.get(pid);
            //get this product
            Product pro = daoPro.get(pid);
            //Construct a bill detail
            double buyPrice = pro.getPrice();
            double subtotal = buyPrice * amount;
            BillDetail bd = new BillDetail(bid, pid, amount, buyPrice, subtotal);
            //Confirm buying
            daoBd.add(bd);
            //Reduce stock of items
            int oldQuantity = pro.getQuantity();
            pro.setQuantity(oldQuantity - amount);
            daoPro.update(pro);
            //add to total
            totalMoney += subtotal;
        }
        //Update the previous bill with the total amount
        bill.setTotalMoney(totalMoney);
        daoBill.update(bill);
        //Wipe cart
        daoPro.clearCart(session);
    }

    private void register(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String submit = request.getParameter("submit");
        if (submit == null) {
            request.getRequestDispatcher("client/register.jsp").forward(request, response);
        } else {
            DAOCustomer dao = new DAOCustomer();
            String username = request.getParameter("username");
            String password = request.getParameter("password");
            String name = request.getParameter("name");
            String address = request.getParameter("address");
            String phone = request.getParameter("phone");
            Customer cus = new Customer(username, name, username, password, address, 1, phone, 0);
            int n = dao.add(cus);
            if (n == 1) {
                request.getRequestDispatcher("client/login.jsp").forward(request, response);
            } else {
                request.getRequestDispatcher("client/register.jsp").forward(request, response);
            }
        }
    }

    public void clearCart(HttpSession session) {
        session.removeAttribute("cart");
    }

}
