package connect;

import java.sql.PreparedStatement;
import java.util.Vector;

public abstract class DAOEntity<E> extends DBConnect {

    public abstract int add(E entity);

    public abstract int update(E entity);

    public abstract Vector<E> getAll();

    public abstract Vector<E> getAll(String sql);

    public abstract Vector<E> getAll(PreparedStatement statement);

    public int remove(String s) {
        throw new UnsupportedOperationException("Does not implement.");
    }

    public int remove(String s1, String s2) {
        throw new UnsupportedOperationException("Does not implement.");
    }

}
