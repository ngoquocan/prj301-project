package connect;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.logging.Level;
import java.util.logging.Logger;

public class DBConnect {
    
    public Connection conn = null;
    
    public DBConnect(String URL,String userName,String password){
        try {
            //driver
            Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");
            //connect
            conn=DriverManager.getConnection(URL, userName, password);
            System.out.println("Connected");
        } catch (ClassNotFoundException ex) {
            ex.printStackTrace();      
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
   }
    public DBConnect() {
        this("jdbc:sqlserver://localhost:1433;databaseName=FinalProjectShop",
                "sa","123456");
    }
    
    public ResultSet getData(String sql){
        ResultSet rs = null;
        try {
            Statement state = conn.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE,
                    ResultSet.CONCUR_UPDATABLE);
            rs = state.executeQuery(sql);
        } catch (SQLException ex) {
            Logger.getLogger(DBConnect.class.getName()).log(Level.SEVERE, null, ex);
        }
        return rs;
    }
    
    public ResultSet getData(PreparedStatement statement){
        ResultSet rs = null;
        try {
            rs = statement.executeQuery();
        } catch (SQLException ex) {
            Logger.getLogger(DBConnect.class.getName()).log(Level.SEVERE, null, ex);
        }
        return rs;
    }
    
    public PreparedStatement getPrep(String sql) throws SQLException{
        return conn.prepareStatement(sql);
    }
    
    public static void main(String[] args) {
        new DBConnect();
    }
   
}
